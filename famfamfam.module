<?php

/**
 * @file
 * Core functions for the famfamfam.com icons module.
 */

/**
 * Implements hook_icon_providers().
 */
function famfamfam_icon_providers() {
  $providers['famfamfam'] = [
    'title' => $this->t('famfamfam.com'),
    'url'   => 'http://www.famfamfam.com',
  ];

  return $providers;
}

/**
 * Implements hook_icon_bundles().
 */
function famfamfam_icon_bundles() {
  $bundles = [];
  $packs   = array(
    'flag' => array('subdir' => '/png'),
    'mini' => array('extension' => 'gif'),
    'mint' => array('subdir' => '/icons'),
    'silk' => array('subdir' => '/icons'),
  );

  foreach ($packs as $name => $pack) {
    $directory = "famfamfam_{$name}_icons";
    if (\Drupal::moduleHandler()->moduleExists('libraries')) {
      $directory = libraries_get_path($directory);
    }
    else {
      $directory = "sites/all/libraries/{$directory}";
    }

    // Add subdirectory if provided.
    if (isset($pack['subdir'])) {
      $directory = "{$directory}{$pack['subdir']}";
    }

    if (is_dir($directory)) {
      $extension = isset($pack['extension']) ? $pack['extension'] : 'png';
      $bundles["famfamfam_{$name}_icons"] = array(
        'render'   => 'image',
        'path'     => $directory,
        'provider' => 'famfamfam',
        'title'    => $this->t('famfamfam.com: @title icons', array('@title' => ucfirst($name))),
        'settings' => array(
          'extension' => $extension,
        ),
      );

      foreach (file_scan_directory($directory, "/{$extension}/") as $file) {
        $bundles["famfamfam_{$name}_icons"]['icons'][$file->name] = $file->name;
      }
    }
  }

  return $bundles;
}
